module tp1

go 1.16

require (
	github.com/go-openapi/strfmt v0.20.3 // indirect
	github.com/jedib0t/go-pretty v4.3.0+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	github.com/vbauerster/mpb/v7 v7.1.5
)
