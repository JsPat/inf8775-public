# INF8775 - TP1
The tp1 repo for INF8775 

# Command
```sh
make build      # compile binary
make gen-file   # generate matrix file
make stats      # generate stats (can take up to 30 minutes to complete)
make clean      # remove all generated file
```

# [tp.sh](tp.sh)
[tp.sh](tp.sh) is just a interface for ./bin/tp. Calling it would be the same as calling ./bin/tp
```sh
./tp.sh <args>
```
Is the same as
```sh
./bin/tp <args>
```

# Usage

## Root command
```
Usage:
  tp [flags]
  tp [command]

Available Commands:
  completion  generate the autocompletion script for the specified shell
  help        Help about any command
  stats       Create stats for tp1

Flags:
  -a, --algorithm string   specified which algorithm to use for the multiplication {conv, strassen, strassenSeuil}
      --e1 string          The file containing the matrix 1 informations
      --e2 string          The file containing the matrix 2 informations
  -h, --help               help for tp
  -p, --print-result       If set, print the result matrix with no other logs
      --threshold int      threshold to use whith strseenSeuil algorithm (default 64)
  -t, --time               If set, print the execution time in ms
  -v, --version            version for tp

Use "tp [command] --help" for more information about a command.
```

## Stats command
```
Create stats for tp1 with different matrix for different size using each algorithm

Usage:
  tp stats [flags]

Flags:
  -d, --data string        The folder containing the "exX_X" files (default "./data")
  -h, --help               help for stats
  -m, --max int            The size of the biggest matrix (eg. 2^10 = 1024) (default 10)
  -l, --min int            The size of the smallest matrix (eg. 2^5 = 32) (default 5)
  -n, --nMatrix int        The number of matrix for each size (default 5)
  -r, --resultDir string   The folder to write out the analysis result in txt and csv format (default "./results")
```


# Project Hierarchy
- bin/
    - Contain the compiled binairy
    - get removed by ```make clean```
- data/
    - Contain all the generate matrix when using ```make gen-file```
    - get removed by ```make clean```
- results/
    - Contain stats result after completing ```make stats```
    - get removed by ```make clean```
- scripts/
    - Contain the python script used by ```make gen-file```
- src/
    - Contain the source file of the project application
- src/cmd/
    - Contains the defintion for the availbale command
- src/Matrix/
    - Contains the defintion for the matrix struct used accros the application
- [main.go](main.go)
    - Entry point of the applcation
- [go.mod](go.mod) & [go.sum](go.sum)
    - golang specific project files
- [tp.sh](tp.sh)
    - Interface to be use by the corrector
- [tp1-a21.odt](tp1-a21.odt)
    - Text report for this tp
