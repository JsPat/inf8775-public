package cmd

import (
	"fmt"
	"time"
	"tp1/src/matrix"

	"github.com/sirupsen/logrus"
)

// rootMain is the entry point for the main command.
func rootMain() error {
	// Reading matrix (m1 and m2) from files
	m1, err := matrix.NewMatrixFromFile(matrixFile1)
	if err != nil {
		return err
	}
	m2, err := matrix.NewMatrixFromFile(matrixFile2)
	if err != nil {
		return err
	}

	// Verify that both matrix has the same size
	if m1.Size() != m2.Size() {
		return fmt.Errorf("matrix does not have the same size")
	}

	// Prepare multiplication
	// Log info
	entry := logrus.WithField("algorithm", algo)
	if algo == strassen || algo == strassenSeuil {
		entry = entry.WithField("seuil", threshold)
	}
	entry.Info("Starting matrix multiplication")

	// Useful variable
	var result *matrix.Matrix
	start := time.Now()

	// Execute multiplication
	switch algo {
	case conventionnal:
		result = m1.Mul(m2)
	case strassen:
		result = matrix.MultStrassen(m1, m2, 1)
	default: // strassen with a given threshold
		result = matrix.MultStrassen(m1, m2, threshold)
	}

	// Analyse multiplication time
	execTime := time.Since(start)
	logrus.WithField("exec-time", execTime).Info("Matrix multiplication done")

	// Print information if requested
	if printResult {
		fmt.Println(result)
	}

	if printExecutionTime {
		fmt.Printf("%.2f\n", float32(execTime.Microseconds())/1000)
	}

	return nil
}
