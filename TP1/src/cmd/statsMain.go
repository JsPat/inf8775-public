package cmd

import (
	"fmt"
	"math"
	"os"
	"path"
	"sync"
	"time"
	"tp1/src/matrix"

	"github.com/jedib0t/go-pretty/table"
	"github.com/jedib0t/go-pretty/text"
	"github.com/sirupsen/logrus"
	"github.com/vbauerster/mpb/v7"
	"github.com/vbauerster/mpb/v7/decor"
)

// format for the matrix name
const matrixFileNameTemplate = "ex%d_%d"

// mulStats is a struct containing statistics information about matrix multiplication
type mulStats struct {
	execTime time.Duration
	m1       *matrix.Matrix
	m2       *matrix.Matrix
}

// currentData is a struct containing information about the current iteration used to display better progressbar
type currentData struct {
	m1 string
	m2 string
}

// update set new matrix to the struct
func (t *currentData) update(m1 string, m2 string) { t.m1 = m1; t.m2 = m2 }

// show return a format string of the wanted information about this iteration
func (t *currentData) show(decor.Statistics) string { return fmt.Sprintf(" | %s * %s", t.m1, t.m2) }

// statsMain is the entry point for the main command.
func statsMain(minSize, maxSize, nMatrixPerSize int, dataDir string) error {
	// Initialise all matrix
	allMatrix, err := readAllMatrix(minSize, maxSize, nMatrixPerSize, dataDir)
	if err != nil {
		return err
	}
	logrus.Info("All matrix have been loaded")
	logrus.Info("Executing multiplication using different algorithm")
	fmt.Println()

	// Create multi thread progressbar
	wg := &sync.WaitGroup{}
	p := mpb.New(mpb.WithWaitGroup(wg))
	wg.Add(2)

	// Launch the actual thread where each of them calculate all matrix multiplication using 1 algorithm
	gatherStats(allMatrix, conventionnal, nMatrixPerSize, 0, wg, p) // conventionnal algorithm
	gatherStats(allMatrix, strassen, nMatrixPerSize, 0, wg, p)      // strassen algorithm with threshold of 1

	// strassen with threshold up to maxsize/2
	for i := 1; i <= maxSize-1; i++ {
		wg.Add(1)
		seuil := int(math.Pow(2, float64(i)))
		gatherStats(allMatrix, strassenSeuil, nMatrixPerSize, seuil, wg, p)
	}

	// Wait for all thread to finish
	p.Wait()
	return nil
}

// readAllMatrix definitions
func readAllMatrix(minSize, maxSize, nMatrixPerSize int, dataDir string) ([][]*matrix.Matrix, error) {
	allMatrix := make([][]*matrix.Matrix, 0, 6)

	for size := minSize; size <= maxSize; size++ {
		matrixOfSameSize := make([]*matrix.Matrix, nMatrixPerSize)

		for i := 0; i < nMatrixPerSize; i++ {
			// Obtain the file containing the matrix definition
			file := path.Join(dataDir, fmt.Sprintf(matrixFileNameTemplate, size, i))

			// Create the matrix from file
			m, err := matrix.NewMatrixFromFile(file)
			if err != nil {
				return nil, err
			}
			matrixOfSameSize[i] = m
		}

		allMatrix = append(allMatrix, matrixOfSameSize)
	}
	return allMatrix, nil
}

// Execute algorithm and output stats t the result directory
func gatherStats(allMatrix [][]*matrix.Matrix, algo algorithm, nMatrixPerSize int, threshold int, wg *sync.WaitGroup, p *mpb.Progress) {
	if algo == strassen {
		threshold = 1
	}

	bar, t, statsWriter, outFilePrefix := initThread(p, algo, threshold) // create new progess bar

	// launch algorithm in separated thread
	go func() {
		// Execute all multiplication
		mulStats := mulAllMatrix(allMatrix, algo, nMatrixPerSize, threshold, bar, t)

		// add time to table
		for _, sizeStats := range mulStats {
			var avg time.Duration
			size := sizeStats[0].m1.Size()

			for _, stats := range sizeStats {
				avg += stats.execTime
				// add individual row
				statsWriter.AppendRow(table.Row{
					fmt.Sprintf("%s * %s", stats.m1.Name(), stats.m2.Name()),
					fmt.Sprintf("%d", size),
					stats.execTime.Microseconds(),
				})
			}

			// add average
			statsWriter.AppendFooter(table.Row{
				"AVERAGE",
				fmt.Sprintf("%d", size),
				(avg / time.Duration(len(sizeStats))).Microseconds(),
			})
		}

		// write result files
		writeStats(outFilePrefix+".txt", []byte(statsWriter.Render()))
		writeStats(outFilePrefix+".csv", []byte(statsWriter.RenderCSV()))
		wg.Done()
	}()
}

// Execute the actual multiplication for all matrix
func mulAllMatrix(allMatrix [][]*matrix.Matrix, algo algorithm, nMatrixPerSize int, threshold int, bar *mpb.Bar, t *currentData) [][]mulStats {
	timeStats := make([][]mulStats, len(allMatrix))

	for i := range allMatrix {
		for m1_index := 0; m1_index < nMatrixPerSize; m1_index++ {
			for m2_index := m1_index + 1; m2_index < nMatrixPerSize; m2_index++ {
				m1 := allMatrix[i][m1_index]
				m2 := allMatrix[i][m2_index]

				// Execute multiplication
				start := time.Now()
				switch algo {
				case conventionnal:
					m1.Mul(m2)
				case strassen, strassenSeuil:
					matrix.MultStrassen(m1, m2, threshold)
				}

				// Add stats for this paritcular multiplication
				execTime := time.Since(start)
				timeStats[i] = append(timeStats[i], mulStats{
					execTime: execTime,
					m1:       m1,
					m2:       m2,
				})

				// Update progressbar
				t.update(m1.Name(), m2.Name())
				bar.Increment()
			}
		}
	}
	return timeStats
}

// writeStats output data to given file and logging error any occur without stopping execution
func writeStats(outFile string, data []byte) {
	err := os.WriteFile(
		path.Join(resultDir, outFile),
		data,
		os.ModePerm,
	)
	if err != nil {
		logrus.WithError(err).Error("Cannot output markdown stats result")
	}
}

// initThread create useful ressource used by idividual thread
func initThread(p *mpb.Progress, algo algorithm, threshold int) (*mpb.Bar, *currentData, table.Writer, string) {
	bar, t := createBar(p, algo, threshold)
	table, outFile := createPrettyTable(algo, threshold)
	return bar, t, table, outFile
}

// createPrettyTable create the table used to output data into files
func createPrettyTable(algo algorithm, threshold int) (table.Writer, string) {
	// Pretty table to be printed
	statsWriter := table.NewWriter()
	var outFilePrefix string

	// set table title based on algorithme
	switch algo {
	case conventionnal:
		statsWriter.SetTitle("Execution time for matrix multiplication\nusing conventionnal algorithm")
		outFilePrefix = "conventionnal"
	case strassen:
		threshold = 1
		fallthrough
	case strassenSeuil:
		statsWriter.SetTitle(fmt.Sprintf("Execution time for matrix multiplication\nusing strassen algorithm (threshold = %d)", threshold))
		outFilePrefix = fmt.Sprintf("strassen_%d", threshold)
	}

	statsWriter.AppendHeader(table.Row{"Operation", "Matrix size", "Execution time (us)"})
	statsWriter.Style().Format.Footer = text.FormatDefault

	return statsWriter, outFilePrefix
}

// createBar create the progress bar used to display progress
func createBar(p *mpb.Progress, algo algorithm, threshold int) (*mpb.Bar, *currentData) {
	var barName string
	if algo == conventionnal {
		barName = string(conventionnal)
	} else {
		barName = fmt.Sprintf("%s (seuil = %d)", strassen, threshold)

	}

	cd := &currentData{}

	bar := p.AddBar(int64(nMultiplication()),
		// The informations befor the actual bar
		mpb.PrependDecorators(
			decor.Name(barName, decor.WC{C: decor.DSyncWidthR}),
			decor.OnComplete(
				decor.Any(cd.show), " | FINISHED",
			),
		),

		// Remove the bar when completed
		mpb.BarFillerClearOnComplete(),

		// The informations after the actual bar
		mpb.AppendDecorators(
			decor.OnComplete(
				decor.CountersNoUnit("%d/%d "), "",
			),
			decor.Elapsed(decor.ET_STYLE_GO),
		),
	)
	return bar, cd
}

// return the number of multiplication for 1 algorithm
func nMultiplication() int {
	n1Size := 0
	for i := nMatrixPerSize - 1; i > 0; i-- {
		n1Size += i
	}
	return (maxSize - minSize + 1) * n1Size
}
