package cmd

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// Variable to hold flags values
var (
	dataDir        string
	resultDir      string
	minSize        int
	maxSize        int
	nMatrixPerSize int
)

// Defining the root command
var Stats = &cobra.Command{
	Use:   "stats",
	Short: "Create stats for tp1",
	Long:  "Create stats for tp1 with different matrix for different size using each algorithm",

	// Create the resultDir if it doesn't exist
	PreRunE: func(cmd *cobra.Command, args []string) error {
		err := os.MkdirAll(resultDir, os.ModePerm)
		if err != nil {
			return fmt.Errorf("cannot create result dir: %v", err)
		}
		return nil
	},

	// Execute the actual stats command
	Run: func(cmd *cobra.Command, args []string) {
		err := statsMain(minSize, maxSize, nMatrixPerSize, dataDir)
		if err != nil {
			logrus.Errorf("%v", err)
			cmd.Usage()
		}
	},
}

// addStatsCommand add the stats sub command to the root command
func addStatsCommand(root *cobra.Command) {
	// Min size flag definition
	Stats.Flags().IntVarP(
		&minSize,
		"min", "l",
		5,
		"The size of the smallest matrix (eg. 2^5 = 32)",
	)

	// Max size flag definition
	Stats.Flags().IntVarP(
		&maxSize,
		"max", "m",
		10,
		"The size of the biggest matrix (eg. 2^10 = 1024)",
	)

	// Number of matrix flag definition
	Stats.Flags().IntVarP(
		&nMatrixPerSize,
		"nMatrix", "n",
		5,
		"The number of matrix for each size",
	)

	// Data folder flag definition
	Stats.Flags().StringVarP(
		&dataDir,
		"data", "d",
		"./data",
		"The folder containing the \"exX_X\" files",
	)

	// Result folder definition
	Stats.Flags().StringVarP(
		&resultDir,
		"resultDir", "r",
		"./results",
		"The folder to write out the analysis result in txt and csv format",
	)

	// Add actual command to the definition
	root.AddCommand(Stats)
}
