package cmd

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// Variable to hold flags values
var (
	algo               algorithm
	rawAlgo            string
	matrixFile1        string
	matrixFile2        string
	printResult        bool
	printExecutionTime bool
	threshold          int
)

// Defining the root command
var Root = &cobra.Command{
	Use:     "tp",
	Short:   "Start tp program",
	Long:    "Execute the INF8775 tp1 program that compare classic matrix multiplication with strassen algorithm",
	Version: "v1.0.0",

	// Initiliase logger and verify the algo flags
	PreRunE: func(cmd *cobra.Command, args []string) error {
		// Validate algo flags
		switch a := algorithm(rawAlgo); a {
		case strassen:
			threshold = 1
			fallthrough
		case strassenSeuil, conventionnal:
			algo = a
		default:
			return fmt.Errorf("unknown algorithm: \"%v\"", rawAlgo)
		}

		// No information log to be print when one of this flag is set
		if printExecutionTime || printResult {
			logrus.SetLevel(logrus.ErrorLevel)
		}

		return nil
	},

	// The actual root command
	Run: func(cmd *cobra.Command, args []string) {
		err := rootMain()
		if err != nil {
			logrus.Errorf("%v", err)
			cmd.Usage()
		}
	},
}

// Initialise root command and flags
func init() {
	// Change -e1 to --e1 and -e2 to --e2 for cobra args parser to satisfy the tp.sh interface
	for i, arg := range os.Args {
		if arg == "-e1" || arg == "-e2" {
			os.Args[i] = "-" + arg
		}
	}

	// Algo flags definition
	Root.Flags().StringVarP(
		&rawAlgo,
		"algorithm",
		"a",
		"",
		fmt.Sprintf("specified which algorithm to use for the multiplication {%s, %s, %s}", conventionnal, strassen, strassenSeuil),
	)

	// Print result matrix flag definition
	Root.Flags().BoolVarP(
		&printResult,
		"print-result",
		"p",
		false,
		"If set, print the result matrix with no other logs",
	)

	// Executoin time flag definition
	Root.Flags().BoolVarP(
		&printExecutionTime,
		"time",
		"t",
		false,
		"If set, print the execution time in ms with no other logs",
	)

	// Matrix 1 input file flag definition
	Root.Flags().StringVar(
		&matrixFile1,
		"e1",
		"",
		"The file containing the matrix 1 informations (can be used with only 1 \"-\" eg: -e1)",
	)

	// Matrix e2 input file flag definition
	Root.Flags().StringVar(
		&matrixFile2,
		"e2",
		"",
		"The file containing the matrix 2 informations (can be used with only 1 \"-\" eg: -e2)",
	)

	// Threshold falg definition for strassenSeuil
	Root.Flags().IntVar(
		&threshold,
		"threshold",
		128,
		"threshold to use whith strseenSeuil algorithm",
	)

	Root.MarkFlagRequired("algorithm")
	Root.MarkFlagRequired("e1")
	Root.MarkFlagRequired("e2")

	// Add stats sub commmand
	addStatsCommand(Root)
}
