package cmd

// Enum of possible algorithm
type algorithm string

const (
	conventionnal = "conv"
	strassen      = "strassen"
	strassenSeuil = "strassenSeuil"
)
