package matrix

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

// Matrix is a struct containing information about a matrix defintion
type Matrix struct {
	name string
	data [][]int
	size int
}

// NewMatrixFromFile read a matrix defintion from text file following the format provided
func NewMatrixFromFile(fileName string) (*Matrix, error) {
	// Open file in read-only mode
	file, err := os.Open(fileName)
	if err != nil {
		return nil, fmt.Errorf("cannot open file: %v", err)
	}
	defer file.Close()

	// Create a new scanner to read file line by line
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	// Read matrix size
	if !scanner.Scan() {
		return nil, fmt.Errorf("empty File")

	}

	size, err := strconv.Atoi(scanner.Text())
	if err != nil {
		return nil, fmt.Errorf("invalid file format: %v", err)
	}
	size = int(math.Pow(2, float64(size)))
	data := make([][]int, size)

	// Read file line by line and put value read into data
	for i := 0; scanner.Scan(); i++ {
		data[i] = make([]int, size)                // creat each row of the matrix
		vals := strings.Split(scanner.Text(), " ") // split on space

		// Cast every value from string to int
		for j, val := range vals {
			data[i][j], err = strconv.Atoi(val)
			if err != nil {
				return nil, fmt.Errorf("invalid file format: %v", err)
			}
		}
	}

	_, name := path.Split(fileName) // the name of this matrix is the name of the file

	// log success
	logrus.
		WithField("name", name).
		WithField("size", fmt.Sprintf("%dx%d", size, size)).
		Info("Matrix succesfully created")

	// create the actual matrix object and return it
	return &Matrix{
		name: name,
		size: size,
		data: data,
	}, nil
}

// newEmptyMatrix create an empty matrix of the given size and initialise data
func newEmptyMatrix(size int) *Matrix {
	data := make([][]int, size)
	for i := 0; i < size; i++ {
		data[i] = make([]int, size)
	}

	return &Matrix{
		size: size,
		data: data,
	}
}

// Size return the size of the matrix
func (m *Matrix) Size() int {
	return m.size
}

func (m *Matrix) Name() string {
	return m.name
}

// return the string representation of this matrix
func (m *Matrix) String() string {
	allRows := make([]string, m.size)
	stringRow := make([]string, m.size)

	for i, row := range m.data {
		for j, val := range row {
			stringRow[j] = strconv.Itoa(val)
		}
		allRows[i] = strings.Join(stringRow, "\t")
	}

	return strings.Join(allRows, "\n")
}

// Mul is the implementation of the classic multiplication
func (m1 *Matrix) Mul(m2 *Matrix) *Matrix {
	size := m1.size
	result := newEmptyMatrix(size)

	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			result.data[i][j] = 0
			for k := 0; k < size; k++ {
				result.data[i][j] += m1.data[i][k] * m2.data[k][j]
			}
		}
	}

	return result
}

// add the second matrix to the first 1
func (m1 *Matrix) add(m2 *Matrix) *Matrix {
	result := newEmptyMatrix(m1.size)

	for i := range result.data {
		for j := range result.data {
			result.data[i][j] = m1.data[i][j] + m2.data[i][j]
		}
	}

	return result
}

// sub the second matrix from the first one
func (m1 *Matrix) sub(m2 *Matrix) *Matrix {
	result := newEmptyMatrix(m1.size)

	for i := range result.data {
		for j := range result.data {
			result.data[i][j] = m1.data[i][j] - m2.data[i][j]
		}
	}

	return result
}

// split the matrix into 4 equal in size matrix
func (m *Matrix) split() (m11, m12, m21, m22 *Matrix) {
	size := m.size / 2

	// m11
	m11Data := make([][]int, size)
	for i := range m11Data {
		m11Data[i] = m.data[i][:size]
	}
	m11 = &Matrix{
		size: size,
		data: m11Data,
	}

	// m12
	m12Data := make([][]int, size)
	for i := range m12Data {
		m12Data[i] = m.data[i][size:]
	}
	m12 = &Matrix{
		size: size,
		data: m12Data,
	}

	// m21
	m21Data := make([][]int, size)
	for i := range m21Data {
		m21Data[i] = m.data[size+i][:size]
	}
	m21 = &Matrix{
		size: size,
		data: m21Data,
	}

	// m22
	m22Data := make([][]int, size)
	for i := range m22Data {
		m22Data[i] = m.data[size+i][size:]
	}
	m22 = &Matrix{
		size: size,
		data: m22Data,
	}
	return
}

// merge combine 4 smaller matrix into 1
//	| M11 M12 |
//	| M21 M22 |
func merge(m11, m12, m21, m22 *Matrix) *Matrix {
	size := m11.size * 2
	result := newEmptyMatrix(size)

	for i := 0; i < m11.size; i++ {
		result.data[i] = append(m11.data[i], m12.data[i]...)
	}

	for i := 0; i < m21.size; i++ {
		result.data[m21.size+i] = append(m21.data[i], m22.data[i]...)
	}

	return result
}

// Equal return true if 2 matrix are the same
func (m1 *Matrix) Equal(m2 *Matrix) bool {
	if m1.size != m2.size {
		return false
	}

	for i := range m1.data {
		for j := range m1.data[i] {
			if m1.data[i][j] != m2.data[i][j] {
				return false
			}
		}
	}

	return true
}

// MulStrassen multiply 2 matrix using the strassen algorithm describe in the cours
func MultStrassen(a, b *Matrix, threshold int) *Matrix {
	size := a.size

	if size <= threshold {
		return a.Mul(b)
	}

	a11, a12, a21, a22 := a.split()
	b11, b12, b21, b22 := b.split()

	M1 := MultStrassen(a11.add(a22), b11.add(b22), threshold) // (A11 + A22) * (B11, B22)
	M2 := MultStrassen(a21.add(a22), b11, threshold)          // (A21 + A22) * B11
	M3 := MultStrassen(a11, b12.sub(b22), threshold)          // A11 * (B12 - B22)
	M4 := MultStrassen(a22, b21.sub(b11), threshold)          // A22 * (B21 - B11)
	M5 := MultStrassen(a11.add(a12), b22, threshold)          // (A11 + A12) * B22
	M6 := MultStrassen(a21.sub(a11), b11.add(b12), threshold) // (A21 - A11) * (B11 + B12)
	M7 := MultStrassen(a12.sub(a22), b21.add(b22), threshold) // (A12 - A22) * (B21 + B22)

	C11 := M1.add(M4).sub(M5).add(M7) // M1 + M4 - M5 + M7
	C12 := M3.add(M5)                 // M3 + M5
	C21 := M2.add(M4)                 // M2 + M4
	C22 := M1.sub(M2).add(M3).add(M6) // M1 - M2 + M3 + M6

	return merge(C11, C12, C21, C22)
}
