package main

import (
	"os"
	"tp1/src/cmd"
)

// Program entry point
func main() {
	err := cmd.Root.Execute()
	if err != nil {
		os.Exit(1)
	}
}
