from util import *

def glouton_choice(matrix_graph, C): # Space: O(n^2) | Time: O(n^2)
    all_dsat = []  # Space: O(1) | Time: O(1)
    # Inspired from https://stackoverflow.com/questions/7270321/finding-the-index-of-elements-based-on-a-condition-using-python-list-comprehensi
    color_indexes = [i for i in range(len(C)) if C[i] != NO_COLOR]  # Space: O(n) | Time: O(n)
    # Get all DSAT
    for i, row in enumerate(matrix_graph):  # Space: O(1) | Time: O(n^2)
        # Inspired from https://stackoverflow.com/questions/46862408/python-find-count-of-the-elements-of-one-list-in-another-list
        dsat = sum(el in row for el in color_indexes) # Space: O(1) | Time: O(n)
        # Don't take into account a vertex already colorized
        if C[i] != NO_COLOR:
            dsat = 0
        all_dsat.append(dsat)
    # Highest DSAT value
    highest_dsat_value = max(all_dsat) # Space: O(n) | Time: O(n)
    # Keep only vertex with the highest DSAT
    matrix_graph_only_highest_dsat = matrix_graph.copy() # Space: O(n^2) | Time: O(n^2)
    for i, dsat in enumerate(all_dsat): # Space: O(1) | Time: O(n)
        if dsat != highest_dsat_value and C[i] != NO_COLOR:
            matrix_graph_only_highest_dsat[i] = []
    max_vertex_index = max_vertex(matrix_graph_only_highest_dsat) # Space: O(n) | Time: O(3n) ∊ O(n)
    return max_vertex_index

def find_smallest_missing_number(list): # Space: O(n) | Time: O(n)
    numbers = set(list) # Space: O(n) | Time: O(n)
    size = len(numbers) # Space: O(1) | Time: O(1)
    for i in range(0, size): # Space: O(1) | Time: O(n)
        if i not in numbers:
            return i
    return size

def smallest_color(matrix_graph, C, next_vertex_index): # Space: O(n) | Time: O(n)
    already_taken_colors = [] # Space: O(1) | Time: O(1)
    for i in matrix_graph[next_vertex_index]: # Space: O(n) | Time: O(n)
        already_taken_colors.append(C[i])
    color = find_smallest_missing_number(already_taken_colors) # Space: O(n) | Time: O(n)
    return color

def glouton(matrix_graph): # Space: O(n^2) | Time: O(n^3)
    # Initialize C
    C = empty_C(len(matrix_graph)) # Space: O(n) | Time: O(n)
    # Get vertex with highest degree
    index_max_vertex = max_vertex(matrix_graph) # Space: O(n) | Time: O(3n) ∊ O(n)
    # Set 0 to the vertex with highest degree
    C[index_max_vertex] = 0 # Space: O(1) | Time: O(1)
    # Set 0 to all detached vertex
    colorize_detached_vertex(matrix_graph, C) # Space: O(1) | Time: O(n)
    while(NO_COLOR in C): # Space: O(n^2) | Time: O(n^3)
        # Choose next vertex to color
        next_vertex_index = glouton_choice(matrix_graph, C) # Space: O(n^2) | Time: O(n^2)
        # Color next vertex with the smallest color
        C[next_vertex_index] = smallest_color(matrix_graph, C, next_vertex_index) # Space: O(n) | Time: O(n)
    return C






