from glouton import glouton, glouton_choice
from util import *


def possible_color(color, index, matrix_graph, C): # Space: O(1) | Time: O(n)
    for node in matrix_graph[index]: # Space: O(1) | Time: O(n)
        if C[node] == color:
            return False
    return True

def explore_nodes(matrix_graph, C): # Space: O(n^2) | Time: O(n^2) (because color <= n, so n si worst than color)
    nodes_list = [] # Space: O(1) | Time: O(1)
    index_max_vertex = glouton_choice(matrix_graph, C) # Space: O(n^2) | Time: O(n^2)
    for color in range(0, K(C) + 1): # Space: O(color n) | Time: O(color n)
        if possible_color(color, index_max_vertex, matrix_graph, C):
            C_prime = C.copy() # Space: O(n) | Time: O(n)
            C_prime[index_max_vertex] = color
            nodes_list.append(C_prime)
    return nodes_list

def branch_and_bound(matrix_graph): # Space: O(n^2) | Time: O(e^n)
    current_best_result = glouton(matrix_graph) # Space: O(n^2) | Time: O(n^3)
    upper_bound = K(current_best_result) # Space: O(1) | Time: O(n)
    nodes_stack = [] # Space: O(1) | Time: O(1)
    C = empty_C(len(matrix_graph)) # Space: O(n) | Time: O(n)
    # Get vertex with highest degree
    index_max_vertex = max_vertex(matrix_graph) # Space: O(n) | Time: O(n)
    # Set 0 to the vertex with highest degree
    C[index_max_vertex] = 0 # Space: O(1) | Time: O(1)
    # Set 0 to all detached vertex
    colorize_detached_vertex(matrix_graph, C) # Space: O(1) | Time: O(n)
    nodes_stack.append(C) # Space: O(1) | Time: O(1)
    while nodes_stack: # Space: O(n^2) | Time: O(e^n) source: https://fr.wikipedia.org/wiki/S%C3%A9paration_et_%C3%A9valuation
        C = nodes_stack.pop() # Space: O(1) | Time: O(1)
        # Complete coloration
        if NO_COLOR not in C:
            if K(C) < upper_bound: # Space: O(1) | Time: O(n)
                current_best_result = C.copy() # Space: O(n) | Time: O(n)
                upper_bound = K(C) # Space: O(1) | Time: O(n)
        # Not complete coloration
        elif K(C) < upper_bound:
            for C_prime in explore_nodes(matrix_graph, C): # Space: O(n^2) | Time: O(n^3)
                nodes_stack.append(C_prime) # Space: O(1) | Time: O(1)
    return current_best_result

