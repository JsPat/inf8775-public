import re

NO_COLOR = -1

def K(C): # Space: O(1) | Time: O(n)
    return max(C) + 1 # Space: O(1) | Time: O(n)

def read_instance(file_name): # Space: O(n^2) | Time: O(n^2)
    with open(file_name) as file:
        lines = file.readlines()
        matrix_graph = []
        for line in lines: # Space: O(n^2) | Time: O(n^2)
            if line.startswith("p edge"):
                n_nodes = int(line.split("p edge ")[1].split(" ")[0])
                matrix_graph = [[] for _ in range(n_nodes)] # Space: O(n) | Time: O(n)
            if line.startswith("e "):
                first_node = int(line.split("e ")[1].split(" ")[0]) - 1
                second_node = int(line.split("e ")[1].split(" ")[1]) - 1
                # Add edge in the first direction
                matrix_graph[first_node].append(second_node) # Space: O(1) | Time: O(1)
                # Add edge in the second direction
                matrix_graph[second_node].append(first_node)
        return matrix_graph

def max_vertex(matrix_graph): # Space: O(n) | Time: O(3n) ∊ O(n)
    # Inspired from https://stackoverflow.com/questions/873327/pythons-most-efficient-way-to-choose-longest-string-in-list
    max_lenght, _ = max([(len(x),x) for x in matrix_graph]) # Space: O(n) | Time: O(2n) ∊ O(n)
    for i, row in enumerate(matrix_graph): # Space: O(1) | Time: O(n)
        if len(row) == max_lenght:
            return i

def empty_C(size): # Space: O(n) | Time: O(n)
    # Initialize C
    C = [] # Space: O(1) | Time: O(1)
    for _ in range(size): # Space: O(n) | Time: O(n)
        C.append(NO_COLOR)
    return C

def colorize_detached_vertex(matrix_graph, C): # Space: O(1) | Time: O(n)
    for i, row in enumerate(matrix_graph): # Space: O(1) | Time: O(n)
        if len(row) == 0:
            C[i] = 0

