import glob
from util import *
from glouton import glouton
from tabou import tabou
from branch_and_bound import branch_and_bound
import argparse
import os.path
import timeit

POSSIBLE_ALGORITHM= ["glouton", "branch_bound","tabou"]

def command_line_args():
    # define possible arguments
    parser = argparse.ArgumentParser(
        description='INF8775 TP2', 
        usage='tp.sh -a [glouton | branch_bound | tabou] -e [path_vers_exemplaire] [-p] [-t]'
    )

    parser.add_argument('-a','--algorithm', dest='algo', required=True,
                        type=str, help='Le type d\'algorithm a utiliser (glouton, branch_bound, tabou)')
    
    parser.add_argument('-e','--exemplaire', dest='path', required=True,
                        type=str, help='le chemin vers le fichier contenant l\'exemplaire. Le chemin peut contenir * pour analyser plusieurs fichiers')

    parser.add_argument('-p','--print',dest='print', required=False, default=False,  action='store_true',
                       help='Imprime la réponse de façons concise sans texte superflu')
    
    parser.add_argument('-t','--time', dest='time', required=False, default=False,  action='store_true',
                        help='Imprime le temps d\'execution en ms sans unite ou texte superflu')

    return parser.parse_args(), parser.print_help


if __name__ == "__main__":

    args, print_help = command_line_args()
    
    # check if path exist
    if not os.path.exists(args.path):
        print('Fichiers inexistant')
        print_help()
        exit(1)

    # read all instance files
    all_instances = glob.glob(args.path)
    
    #execute algorithm on all instance provided
    for instance in all_instances:   
        result =[]
        matrix_graph = read_instance(instance)
        start = timeit.default_timer()

        if args.algo == 'glouton':
            result = glouton(matrix_graph)
        elif args.algo == 'branch_bound':
            result = branch_and_bound(matrix_graph)
        elif args.algo == 'tabou':
            result = tabou(matrix_graph)
        else:
            print("Algorithme inconnue")
            print_help()
            exit(1)
        
        total_time = timeit.default_timer() - start
        
        if args.print:
            print(max(result)+1)
            print(*result, sep=" ")

        if args.time:
            print(total_time * 1000 )

        if not args.time and not args.print:
            print('{:<21}{}'.format('number of color:', max(result)+1))
            print('{:<21}'.format('coloration:'), end='')
            print(*result, sep=" ")
            print('{:<21}{}'.format('execution time (ms):', total_time * 1000 ))