from glouton import glouton
from util import *
from random import randrange


N = 100 # This value can be modified to optimize the tabou algorithm
G = 10
A = 2


def minimize_conflicts(max_color, neighbors, C): # Space: O(max_color) | Time: O(n) (because max_color <= n, so n si worst than max_color)
    # All possible colors
    conflict_counter = {i: 0 for i in range(max_color + 1)} # Space: O(max_color) | Time: O(max_color)
    # Increment conflict counter for each neighbor's color
    for neighbor in neighbors: # Space: O(1) | Time: O(n)
        conflict_counter[C[neighbor]] += 1
    # Return the color for which there is the less conflicts
    return min(conflict_counter, key=conflict_counter.get) # Space: O(1) | Time: O(n)


# Minimize the colors used in C
def colors_reduction(matrix_graph, C): # Space: O(1) | Time: O((n^2)/color)
    C = C.copy() # Space: O(n) | Time: O(n)
    n_colors = K(C) # Space: O(1) | Time: O(n)
    for i, color in enumerate(C): # Space: O(1) | Time: O((n^2)/color)
        if color == n_colors - 1: # Space: O(1) | Time: O(n/color) (because we do "color" iterations before going inside the if)
            C[i] = minimize_conflicts(n_colors - 2, matrix_graph[i], C) # Space: O(n) | Time: O(n)
    return C


# Count the total number of conflicts in the matrix graph with the coloration
def conflicts_counter(matrix_graph, C): # Space: O(n) | Time: O(n^2)
    # Total number of conflicts
    n_conflicts = 0
    # For each node: True if it has at least one conflict, False otherwise
    conflicts = [] # Space: O(1) | Time: O(1)
    for i, neighbors in enumerate(matrix_graph): # Space: O(n) | Time: O(n^2)
        conflicts.append(False) # Space: O(1) | Time: O(1)
        for neighbor in neighbors: # Space: O(n) | Time: O(n)
            if C[i] == C[neighbor]: # Space: O(1) | Time: O(1)
                n_conflicts += 1
                conflicts[i] = True
    return (n_conflicts/2, conflicts) # divide n_conflicts, because it's counted twice (ex: 2->4 and 4->2)


def tabou_lifetime(CD): # Space: O(1) | Time: O(1)
    return A * CD + randrange(G) # Space: O(1) | Time: O(1)


# Decrement the lifetime value of an element in tabou list and delete it if it<'s lifetime value is now 0
def clean_tabou_list(tabou_list): # Space: O(color n) | Time: O(color n)
    for dictionnary in tabou_list: # Space: O(color n) | Time: O(color n)
        keys_to_delete = []
        for key in dictionnary: # Space: O(color) | Time: O(color)
            if dictionnary[key] < 1: # Space: O(1) | Time: O(1)
                keys_to_delete.append(key) # Space: O(1) | Time: O(1)
            else:
                dictionnary[key] -= 1
        for key in keys_to_delete: # Space: O(1) | Time: O(n)
            del dictionnary[key]


def find_best_change(matrix_graph, C, tabou_list, CD, conflicts): # Space: O(n^2) | Time: O(n^2 log color log n)
    # Variable initialization
    n_colors = K(C) # Space: O(1) | Time: O(n)
    min_conflicts = CD
    new_conflicts = conflicts
    best_coloration = C.copy() # Space: O(n) | Time: O(n)
    index = 0

    for i in range(len(matrix_graph)): # Space: O(n^2) | Time: O(n^2 log color log n) (because conflicts makes us skip the loop sometimes)
        # This node doesn't have any conflicts, we don't need to change it's color
        if conflicts[i] == False:
            continue
        for color in range(n_colors): # Space: O(n^2) | Time: O(n^2 log color) (because tabou_list makes us skip the loop sometimes)
            # Try possible colors that are not already in the tabou list for this node and are different from the current color
            if color in tabou_list[i] or color == C[i]:
                continue
            # Generate a new possible coloration
            new_coloration = C.copy() # Space: O(n) | Time: O(n)
            new_coloration[i] = color
            # Count the conflicts for this new possible coloration
            n_conflicts, tmp_conflicts = conflicts_counter(matrix_graph, new_coloration) # Space: O(n^2) | Time: O(n^2)
            # Assign this possible coloration to the best coloration if the number of conflicts decreases
            if n_conflicts <= min_conflicts:
                min_conflicts = n_conflicts
                best_coloration = new_coloration
                index = i
                new_conflicts = tmp_conflicts
    color_number = C[index]
    # Add to the tabou list the old coloration of the node that changed it's color
    if not color_number in tabou_list[index]:
        tabou_list[index][color_number] = tabou_lifetime(min_conflicts) # Space: O(1) | Time: O(1)
    return (best_coloration, min_conflicts, new_conflicts)



def tabou_search(matrix_graph, C): # Space: O(n^2) | Time: O(n^2 log color log n)
    # Variable initialization
    n = N
    current_coloration = C.copy() # Space: O(n) | Time: O(n)
    tabou_list = [dict() for _ in C] # Space: O(n) | Time: O(n)
    CD, conflicts = conflicts_counter(matrix_graph, C) # Space: O(n^2) | Time: O(n^2)

    # Try different colorations and return the best when there is no more conflicts or after a specific amount of tries
    while CD > 0: # Space: O(n^2) | Time: O(n^2 log color log n) (and because of the if, this loop is O(100) ∊ O(1))
        current_coloration, CD, conflicts = find_best_change(matrix_graph, current_coloration, tabou_list, CD, conflicts) # Space: O(n^2) | Time: O(n^2 log color log n)
        clean_tabou_list(tabou_list) # Space: O(color n) | Time: O(color n)
        if n <= 0: # Space: O(1) | Time: O(N) = O(100) ∊ O(1) in our case (because we defined N = 100, so it can't be greater than that)
            return None # Tabou search didn't found a new solution
        n = n - 1
    return current_coloration # Tabou search found a new solution


def tabou(matrix_graph): # Space: O(n^2) | Time: O(n^3) (because n^3 is greater than n^2 log color log n (because color <= n))
    current_best_result = glouton(matrix_graph) # Space: O(n^2) | Time: O(n^3)
    # Return the best coloration after all the tabou searches
    while True: # Space: O(n^2) | Time: O(n^2 log color log n)
        current_result = colors_reduction(matrix_graph, current_best_result) # Space: O(1) | Time: O((n^2)/color)
        solution = tabou_search(matrix_graph, current_result) # Space: O(n^2) | Time: O(n^2 log color log n)
        if not solution:# Space: O(1) | Time: O(N) = O(100) ∊ O(1) in our case (because we defined N = 100, so it can't be greater than that)
            return current_best_result
        current_best_result = solution


